package br.edu.udc.ed.arvore.trabalhoDialogoNodoBinario;

import java.util.Scanner;

import br.edu.udc.ed.arvore.NodoBinario;

public class Dialogo {
	public static void main(String args[]) {
		Dialogo dialogo = new Dialogo();
		dialogo.comecar();
	}

	private NodoBinario<String> inicio;

	public Dialogo() {
		this.inicio = new NodoBinario<>("ola voce esta bem ?");
		this.inicio
				.adicionar("esta na semana de provas?")
				.adicionar(" entao acabei de melhorar o seu dia ;D")
				.getPai()
				.adicionar("bem vindo ao clube")
				.getPai().getPai()
				.adicionar("que bom entao vc nao esta fazendo o projeto integrador ;D")
				.adicionar("viu eu sabia so assim pra voce estar feliz ;/")
				.getPai().adicionar("mentiroso nao falo mais com vc");
					
	}
	
	public void comecar() {

		NodoBinario<String> cursor = this.inicio;
		Scanner scaner = new Scanner(System.in);
		
		while (!cursor.externo()) {
			System.out.println(cursor.getElemento() + "(sim = s / nao = n):");

			switch (scaner.nextLine()) {
			case "n":
				cursor = cursor.getNodoEsquerdo();
				break;
			case "s":
				cursor = cursor.getNodoDireito();
				break;
			}
		}
		System.out.println(cursor.getElemento());
		scaner.close();
	}
}
