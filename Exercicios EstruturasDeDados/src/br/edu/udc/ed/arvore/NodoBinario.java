package br.edu.udc.ed.arvore;

import br.edu.udc.ed.colecao.ColecaoIteravel;
import br.edu.udc.ed.lista.vetor.Vetor;

public class NodoBinario<E> extends NodoBinarioAbstrato<E> {

	private NodoBinario<E> nodoEsquerdo;
	private NodoBinario<E> nodoDireito;
	private int tamanhoArvore = 0;

	public NodoBinario(E elementoRaiz, NodoBinario<E> nodoPai) {
		super(elementoRaiz, nodoPai);
	}

	public NodoBinario(E elementoRaiz) {
		super(elementoRaiz);
	}

	@Override
	public NodoBinarioAbstrato<E> getEsquerdo() {
		return this.nodoEsquerdo;
	}

	public NodoBinario<E> getNodoEsquerdo() {
		return nodoEsquerdo;
	}

	public void setNodoEsquerdo(NodoBinario<E> nodoEsquerdo) {
		this.nodoEsquerdo = nodoEsquerdo;
	}

	public NodoBinario<E> getNodoDireito() {
		return nodoDireito;
	}

	public void setNodoDireito(NodoBinario<E> nodoDireito) {
		this.nodoDireito = nodoDireito;
	}

	@Override
	public NodoBinarioAbstrato<E> getDireito() {
		return this.nodoDireito;
	}

	@Override
	public int tamanhoArvore() {
		return this.tamanhoArvore();
	}

	@Override
	public NodoAbstrato<E> adicionar(E elemento) {
		final NodoBinario<E> nodoFilho = new NodoBinario<>(elemento, this);
		if (this.nodoEsquerdo == null) {
			this.nodoEsquerdo = nodoFilho;
		} else if (this.nodoDireito == null) {
			this.nodoDireito = nodoFilho;
		} else {
			throw new IllegalArgumentException("este nodo ja contem dois filhos");
		}
		final NodoBinario<E> raiz = (NodoBinario<E>) super.getRaiz();
		raiz.tamanhoArvore++;
		return (NodoBinario<E>) nodoFilho;

	}

	@Override
	public ColecaoIteravel<NodoAbstrato<E>> getFilhos() {
		final Vetor<NodoAbstrato<E>> filhos = new Vetor<>();

		if (this.getEsquerdo() != null) {
			filhos.adiciona(this.getEsquerdo());
			if (this.getDireito() != null)
				filhos.adiciona(this.getDireito());

		} else if (this.getDireito() != null) {
			throw new IllegalStateException("Arvore impropria. So pode haver um no na direita.");
		}

		return filhos;
	}


	public int getTamanhoArvore() {
		return tamanhoArvore;
	}

	public void setTamanhoArvore(int tamanhoArvore) {
		this.tamanhoArvore = tamanhoArvore;
	}

}
