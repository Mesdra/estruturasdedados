package br.edu.udc.ed.arvore;


import org.junit.Assert;
import org.junit.Test;


public class NodoBinarioTestes {
	
	@Test
	public void adicionaEmNodoBinariodevePassar() {
		final NodoBinario<String> raiz = new NodoBinario<String>("chefe");
		raiz.adicionar("sim");
		raiz.adicionar("nao");
		
		Assert.assertEquals(2, raiz.grau());
	}
	
	@Test
	public void adicionadeveFalhar() {
		final NodoBinario<String> raiz = new NodoBinario<String>("chefe");
		raiz.adicionar("sim");
		raiz.adicionar("nao");
		raiz.getEsquerdo().adicionar("teste");
		
		
		Assert.assertEquals(2, raiz.grau());
	}
	
	
}
