package br.edu.udc.mapa.testes;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.mapa.heroi.Heroi;
import br.edu.udc.mapa.heroi.MapaTabelaHeroi;

public class MapaTabelaHeroisTeste {
	
	@Test
	public void adicionaDevePassar(){
		final MapaTabelaHeroi mapa = new MapaTabelaHeroi();
		final Heroi iroman = new Heroi();
		iroman.setNome("iron Man");
		iroman.setEspecialidade("criar armas");
		iroman.setForca(60F);
		iroman.setFraqueza("egoista");
		iroman.setVoa(true);
		mapa.adiciona("asdasd-asdasd-asdasd", iroman);
		final Heroi hulk = new Heroi();
		hulk.setNome("Hulk");
		hulk.setEspecialidade("demolir");
		hulk.setForca(95F);
		hulk.setHumano(true);
		hulk.setFraqueza("temperamental");
		hulk.setVoa(false);
		
		mapa.adiciona("121212-asdasd-asdasd", hulk);
		
		Assert.assertTrue(mapa.contem("asdasd-asdasd-asdasd"));
		Assert.assertTrue(mapa.contem("121212-asdasd-asdasd"));
		Assert.assertEquals(mapa.tamanho(), 2);
		Assert.assertEquals(iroman,mapa.obtem("asdasd-asdasd-asdasd"));
		Assert.assertEquals(hulk,mapa.obtem("121212-asdasd-asdasd"));
		
		
		
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void removeDeveFalhar(){
		final MapaTabelaHeroi mapa = new MapaTabelaHeroi();
		mapa.remove("Nao Existe");
	}
	
	@Test
	public void removeDevePassar(){
		final MapaTabelaHeroi mapa = new MapaTabelaHeroi();
		final Heroi hulk = new Heroi();
		hulk.setNome("Hulk");
		hulk.setEspecialidade("demolir");
		hulk.setForca(95F);
		hulk.setHumano(true);
		hulk.setFraqueza("temperamental");
		hulk.setVoa(false);
		
		mapa.adiciona("121212-asdasd-asdasd", hulk);
		mapa.remove("121212-asdasd-asdasd");
		Assert.assertEquals(0, mapa.tamanho());
		Assert.assertFalse(mapa.contem("121212-asdasd-asdasd"));
	}
	
	
	@Test
	public void adiciona1000(){
		final MapaTabelaHeroi mapa = new MapaTabelaHeroi();
		final Heroi hulk = new Heroi();
		hulk.setNome("Hulk");
		hulk.setEspecialidade("demolir");
		hulk.setForca(95F);
		hulk.setHumano(true);
		hulk.setFraqueza("temperamental");
		hulk.setVoa(false);
		for(int i=0;i<1000;i++){
			mapa.adiciona("i", hulk);
		}
	}
}
