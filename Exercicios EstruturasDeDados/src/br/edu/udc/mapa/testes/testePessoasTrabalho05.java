package br.edu.udc.mapa.testes;



import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.vetor.Vetor;
import br.edu.udc.mapa.trabalhoMapaPessoas.DocumentoUnico;
import br.edu.udc.mapa.trabalhoMapaPessoas.MapaTabelaPessoas;
import br.edu.udc.mapa.trabalhoMapaPessoas.Pessoas;

public class testePessoasTrabalho05 {
	
	@Test
	public void pegarListaDePessoasdoMapa() {
		MapaTabelaPessoas mapa = new MapaTabelaPessoas();
		Pessoas pessoa = new Pessoas("vinicius", "masc");
		Pessoas pessoa1 = new Pessoas("jo", "fem");
		DocumentoUnico documento  = new DocumentoUnico(45, 33);
		DocumentoUnico documento2  = new DocumentoUnico(60, 23);
		
		mapa.adiciona(documento, pessoa);
		mapa.adiciona(documento2, pessoa1);
		
		Assert.assertEquals(2, mapa.tamanho());
		
		final Vetor<Pessoas> pessoas = mapa.listaTodasasPessoas();
		Assert.assertEquals(2, pessoas.tamanho());
		
	}
	
	@Test
	public void pegarListaDeDocumentosdoMapa() {
		MapaTabelaPessoas mapa = new MapaTabelaPessoas();
		Pessoas pessoa = new Pessoas("vinicius", "masc");
		Pessoas pessoa1 = new Pessoas("jo", "fem");
		DocumentoUnico documento  = new DocumentoUnico(45, 33);
		DocumentoUnico documento2  = new DocumentoUnico(60, 23);
		
		mapa.adiciona(documento, pessoa);
		mapa.adiciona(documento2, pessoa1);
		
		Assert.assertEquals(2, mapa.tamanho());
		
		final Vetor<DocumentoUnico> pessoas = mapa.listaTodososDocumentos();
		Assert.assertEquals(2, pessoas.tamanho());
	}
	
	@Test// teste ja mostra que esta balanceado e imprime os valores de todas as posicoes.
	public void adicionar9000Cadastros() {
		MapaTabelaPessoas mapa = new MapaTabelaPessoas();
		Pessoas pessoa = new Pessoas("vinicius", "masc");
		
		
		
		for (int i = 0; i < 9000 ; i++) {
			final DocumentoUnico documento  = new DocumentoUnico(i, i+5);
			mapa.adiciona(documento, pessoa);
		}
		
		
		
		Assert.assertEquals(9000, mapa.tamanho());
		
		final Vetor<DocumentoUnico> pessoas = mapa.listaTodososDocumentos();
		Assert.assertEquals(9000, pessoas.tamanho());
		mapa.imprimir();
	}
}
