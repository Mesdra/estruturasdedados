package br.edu.udc.mapa.heroi;

import br.edu.udc.ed.vetor.Vetor;

public class MapaTabelaHeroi {
	private Vetor<Vetor<AssociacaoHeroi>> tabela = new Vetor<>();
	private int quantidade = 0 ;
	private final short TAMANHO_MINIMO = 10;
	public MapaTabelaHeroi(){
		for (int i = 0; i < 100; i++) {
			this.tabela.adicionar(new Vetor<AssociacaoHeroi>());
		}
	}
	

	private int calculaIndiceDaTabela(String qrcode){
		return Math.abs(qrcode.hashCode()) % this.tabela.tamanho();
	}

	// tarefa Atividade 03 adicionar com a mesma chave deve trocar o valor inserido
	public void adiciona(String qrcode, Heroi heroi) {
		this.verificaCapacidade();
		if(this.contem(qrcode)){
			final int indice = this.calculaIndiceDaTabela(qrcode);
			final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
			for(int i=0;i < vetor.tamanho();i++){
				final AssociacaoHeroi associacao = vetor.obtem(i);
				if(associacao.getQrcode().equals(qrcode)){
					associacao.setHerio(heroi);
					return;
				}
			}
		}
		
		final int indice = this.calculaIndiceDaTabela(qrcode);
		final Vetor<AssociacaoHeroi>vetor = this.tabela.obtem(indice);
		vetor.adicionar(new AssociacaoHeroi(qrcode, heroi));
		this.quantidade++;
	}
	// verificaCapacidade trabalho 04
		private void verificaCapacidade() {
			int capacidade = this.tabela.tamanho();
			final int carga = this.tamanho() / capacidade;

			if (carga >= 0.75) {
				this.redimencionarTabela(capacidade * 2);
			}
			if (carga <= 0.25) {
				final int novaCapacidade = capacidade / 2;
				if (novaCapacidade > TAMANHO_MINIMO) {
					this.redimencionarTabela(novaCapacidade);
				}

			}

		}
		private void redimencionarTabela(int novaCapacidade) {
			final Vetor<AssociacaoHeroi> objetos = this.todas();
			this.tabela = new Vetor<>();

			for (int i = 0; i < novaCapacidade; i++) {
				this.tabela.adicionar(new Vetor<AssociacaoHeroi>());
			}

			for (int i = 0; i < objetos.tamanho(); i++) {
				final AssociacaoHeroi objeto = objetos.obtem(i);
				final int indice = this.calculaIndiceDaTabela(objeto.getQrcode());
				this.tabela.obtem(indice).adicionar(objeto);
			}
		}
		public Vetor<AssociacaoHeroi> todas() {
			final Vetor<AssociacaoHeroi> todosObjetos = new Vetor<AssociacaoHeroi>();

			for (int i = 0; i < this.tabela.tamanho(); i++) {
				final Vetor<AssociacaoHeroi> palavras = this.tabela.obtem(i);

				for (int j = 0; j < palavras.tamanho(); j++) {
					final AssociacaoHeroi objeto = palavras.obtem(j);
					todosObjetos.adicionar(objeto);
				}
			}
			return todosObjetos;
		}
	public void remove(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);
		
		final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
		for(int i=0;i < vetor.tamanho();i++){
			final AssociacaoHeroi associacao = vetor.obtem(i);
			if(associacao.getQrcode().equals(qrcode)){
				vetor.remove(i);
				this.quantidade--;
				return;
			}
		}
		throw new IllegalArgumentException("nao existe heroi com esta QRcode");
	}

	public Heroi obtem(String qrcode) {
			final int indice  = this.calculaIndiceDaTabela(qrcode);
			final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
			
			for(int i =0 ;i<vetor.tamanho();i++){
				final AssociacaoHeroi associacao = vetor.obtem(i);
				if(associacao.getQrcode().equals(qrcode))
					return associacao.getHerio();
			}
			
			throw new IllegalArgumentException("Nao existe Heroi com esteqrcode");
	}

	public boolean contem(String qrcode) {
		final int indice  = this.calculaIndiceDaTabela(qrcode);
		
		final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
		
		for (int i = 0; i < vetor.tamanho(); i++) {
			final AssociacaoHeroi associacao = vetor.obtem(i);
			if(associacao.getQrcode().equals(qrcode))
				return true;
		}
		return false;
		
	}

	public int tamanho() {
		return this.quantidade;
	}

}
