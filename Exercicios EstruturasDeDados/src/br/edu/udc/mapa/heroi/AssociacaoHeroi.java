package br.edu.udc.mapa.heroi;

class AssociacaoHeroi {
		private String qrcode;
		private Heroi herio;
		
	
		public AssociacaoHeroi(String qrcode, Heroi heroi ){
			this.qrcode = qrcode;
			this.herio = heroi;
		}


		public String getQrcode() {
			return qrcode;
		}

		public Heroi getHerio() {
			return herio;
		}


		public void setQrcode(String qrcode) {
			this.qrcode = qrcode;
		}


		public void setHerio(Heroi herio) {
			this.herio = herio;
		}
		
		
}
