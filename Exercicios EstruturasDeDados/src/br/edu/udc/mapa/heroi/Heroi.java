package br.edu.udc.mapa.heroi;

public class Heroi {
	private String nome;
	private Float forca;
	private String especialidade;
	private String fraqueza;
	private boolean humano;
	private boolean voa;
	
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((especialidade == null) ? 0 : especialidade.hashCode());
		result = prime * result + ((forca == null) ? 0 : forca.hashCode());
		result = prime * result + ((fraqueza == null) ? 0 : fraqueza.hashCode());
		result = prime * result + (humano ? 1231 : 1237);
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + (voa ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Heroi other = (Heroi) obj;
		if (especialidade == null) {
			if (other.especialidade != null)
				return false;
		} else if (!especialidade.equals(other.especialidade))
			return false;
		if (forca == null) {
			if (other.forca != null)
				return false;
		} else if (!forca.equals(other.forca))
			return false;
		if (fraqueza == null) {
			if (other.fraqueza != null)
				return false;
		} else if (!fraqueza.equals(other.fraqueza))
			return false;
		if (humano != other.humano)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (voa != other.voa)
			return false;
		return true;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Float getForca() {
		return forca;
	}
	public void setForca(Float forca) {
		this.forca = forca;
	}
	public String getEspecialidade() {
		return especialidade;
	}
	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}
	public String getFraqueza() {
		return fraqueza;
	}
	public void setFraqueza(String fraqueza) {
		this.fraqueza = fraqueza;
	}
	public boolean isHumano() {
		return humano;
	}
	public void setHumano(boolean humano) {
		this.humano = humano;
	}
	public boolean isVoa() {
		return voa;
	}
	public void setVoa(boolean voa) {
		this.voa = voa;
	}
	
	
}
