package br.edu.udc.mapa.trabalhoMapaPessoas;

public class Pessoas {
	private String nome;
	private String sexo;
	
	public Pessoas(String nome,String sexo){
		this.nome = nome;
		this.sexo = sexo;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String toString(){
		return String.format("Nome = %s / Sexo = %s\n", this.nome,this.sexo);
	}
	
	
}
