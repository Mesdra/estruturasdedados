package br.edu.udc.mapa.trabalhoMapaPessoas;


import br.edu.udc.ed.vetor.Vetor;
import br.edu.udc.mapa.Associacao;
import br.edu.udc.mapa.Mapa;

public class MapaTabelaPessoas extends Mapa<DocumentoUnico,Pessoas>{//trabalho 05 mapa de pessoas 
	

	
	public Vetor<Pessoas> listaTodasasPessoas() {
		Vetor<Pessoas> pessoas = new Vetor<>();
		for (int i = 0; i < this.getTabela().tamanho(); i++) {
			final Vetor<Associacao<DocumentoUnico, Pessoas>> vetor = this.getTabela().obtem(i);

			if (vetor.tamanho() != 0) {
				for (int j = 0; j < vetor.tamanho(); j++) {
					pessoas.adicionar(vetor.obtem(j).getValor());
				}

			}

		}

		return pessoas;
	}
	public Vetor<DocumentoUnico> listaTodososDocumentos() {
		Vetor<DocumentoUnico> documentos = new Vetor<>();
		for (int i = 0; i < this.getTabela().tamanho(); i++) {
			final Vetor<Associacao<DocumentoUnico, Pessoas>> vetor = this.getTabela().obtem(i);

			if (vetor.tamanho() != 0) {
				for (int j = 0; j < vetor.tamanho(); j++) {
					documentos.adicionar(vetor.obtem(j).getChave());
				}

			}

		}

		return documentos;
	}
	@Override
	public String toString(){
		StringBuffer acomulador = new StringBuffer();

		for (int i = 0; i < this.getTabela().tamanho(); i++) {

			final Vetor<Associacao<DocumentoUnico,Pessoas>> vetor = this.getTabela().obtem(i);
			for (int j = 0; j < vetor.tamanho(); j++) {

				final Associacao<DocumentoUnico,Pessoas> acumulador = vetor.obtem(j);
				acomulador.append(acumulador.getValor().toString());
				acomulador.append(acumulador.getChave().toString());
				acomulador.append(String.format(" , "));
			}
			
			acomulador.append(String.format("\n"));
		}

		return acomulador.toString();
	}
	
}
