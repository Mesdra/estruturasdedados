package br.edu.udc.mapa;

import br.edu.udc.ed.vetor.Vetor;

public class Mapa<C,V>{
	private Vetor<Vetor<Associacao<C,V>>> tabela = new Vetor<>();
	private int quantidade = 0 ;
	private final short TAMANHO_MINIMO = 10;
	
	public Mapa(){
		for (int i = 0; i < 100; i++) {
			this.tabela.adicionar(new Vetor<Associacao<C,V>>());
			
		}
	}
	

	private int calculaIndiceDaTabela(C chave){
		return Math.abs(chave.hashCode()) % this.tabela.tamanho();
	}

	// tarefa Atividade 03 adicionar com a mesma chave deve trocar o valor inserido
	public void adiciona(C chave, V valor) {
		this.verificaCapacidade();
		if(this.contem(chave)){
			final int indice = this.calculaIndiceDaTabela(chave);
			final Vetor<Associacao<C,V>> vetor = this.tabela.obtem(indice);
			for(int i=0;i < vetor.tamanho();i++){
				final Associacao<C,V> associacao = vetor.obtem(i);
				if(associacao.getChave().equals(chave)){
					associacao.setValor(valor);
					return;
				}
			}
		}
		
		final int indice = this.calculaIndiceDaTabela(chave);
		final Vetor<Associacao<C,V>>vetor = this.tabela.obtem(indice);
		vetor.adicionar(new Associacao<>(chave, valor));
		this.quantidade++;
	}
	// verificaCapacidade trabalho 04
	private void verificaCapacidade() {
		int capacidade = this.tabela.tamanho();
		final int carga = this.tamanho() / capacidade;

		if (carga >= 0.75) {
			this.redimencionarTabela(capacidade * 2);
		}
		if (carga <= 0.25) {
			final int novaCapacidade = capacidade / 2;
			if (novaCapacidade > TAMANHO_MINIMO) {
				this.redimencionarTabela(novaCapacidade);
			}

		}

	}
	private void redimencionarTabela(int novaCapacidade) {
		final Vetor<Associacao<C,V>> objetos = this.todas();
		this.tabela = new Vetor<>();

		for (int i = 0; i < novaCapacidade; i++) {
			this.tabela.adicionar(new Vetor<Associacao<C,V>>());
		}

		for (int i = 0; i < objetos.tamanho(); i++) {
			final Associacao<C,V> objeto = objetos.obtem(i);
			final int indice = this.calculaIndiceDaTabela(objeto.getChave());
			this.tabela.obtem(indice).adicionar(objeto);
		}
	}
	public Vetor<Associacao<C,V>> todas() {
		final Vetor<Associacao<C,V>> todosObjetos = new Vetor<Associacao<C,V>>();

		for (int i = 0; i < this.tabela.tamanho(); i++) {
			final Vetor<Associacao<C,V>> palavras = this.tabela.obtem(i);

			for (int j = 0; j < palavras.tamanho(); j++) {
				final Associacao<C,V> objeto = palavras.obtem(j);
				todosObjetos.adicionar(objeto);
			}
		}
		return todosObjetos;
	}

	public void remove(C chave) {
		final int indice = this.calculaIndiceDaTabela(chave);
		
		final Vetor<Associacao<C,V>> vetor = this.tabela.obtem(indice);
		for(int i=0;i < vetor.tamanho();i++){
			final Associacao<C,V> associacao = vetor.obtem(i);
			if(associacao.getChave().equals(chave)){
				vetor.remove(i);
				this.quantidade--;
				return;
			}
		}
		throw new IllegalArgumentException("nao existe heroi com esta QRcode");
	}

	public V obtem(C chave) {
			final int indice  = this.calculaIndiceDaTabela(chave);
			final Vetor<Associacao<C,V>> vetor = this.tabela.obtem(indice);
			
			for(int i =0 ;i<vetor.tamanho();i++){
				final Associacao<C,V> associacao = vetor.obtem(i);
				if(associacao.getChave().equals(chave))
					return associacao.getValor();
			}
			
			throw new IllegalArgumentException("Nao existe Heroi com estechave");
	}

	public boolean contem(C chave) {
		final int indice  = this.calculaIndiceDaTabela(chave);
		
		final Vetor<Associacao<C,V>> vetor = this.tabela.obtem(indice);
		
		for (int i = 0; i < vetor.tamanho(); i++) {
			final Associacao<C,V> associacao = vetor.obtem(i);
			if(associacao.getChave().equals(chave))
				return true;
		}
		return false;
		
	}

	public int tamanho() {
		return this.quantidade;
	}
	
	public void imprimir() {
		
		System.out.println("CAPACIDADE"+ this.tamanho());
		
		for (int i = 0; i < this.tabela.tamanho(); i++) {
			final Vetor<Associacao<C,V>> objeto = this.tabela.obtem(i);
			
			if(objeto.tamanho() == 0)
				continue;
			
			System.out.println("CODIGO"+ i + "----TOTAL"+ objeto.tamanho());
			
			for (int j = 0; j < objeto.tamanho(); j++) {
				System.out.println(objeto.obtem(j).getChave().toString()+objeto.obtem(j).getValor().toString());
			}
			
		}
	}


	public Vetor<Vetor<Associacao<C, V>>> getTabela() {
		return tabela;
	}


	public void setTabela(Vetor<Vetor<Associacao<C, V>>> tabela) {
		this.tabela = tabela;
	}


	public int getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

}
