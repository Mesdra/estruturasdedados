package br.edu.udc.mapa;


import br.edu.udc.ed.vetor.Vetor;

public class Mapa<C,V>{
	private Vetor<Vetor<Associacao<C,V>>> tabela = new Vetor<>();
	private int quantidade = 0 ;
	
	public Mapa(){
		for (int i = 0; i < 100; i++) {
			this.tabela.adicionar(new Vetor<Associacao<C,V>>());
			
		}
	}
	

	private int calculaIndiceDaTabela(C chave){
		return Math.abs(chave.hashCode()) % this.tabela.tamanho();
	}

	
	public void adiciona(C chave, V valor) {
		if(this.contem(chave))
			this.remove(chave);
		
		final int indice = this.calculaIndiceDaTabela(chave);
		final Vetor<Associacao<C,V>>vetor = this.tabela.obtem(indice);
		vetor.adicionar(new Associacao<>(chave, valor));
		this.quantidade++;
	}

	public void remove(C chave) {
		final int indice = this.calculaIndiceDaTabela(chave);
		
		final Vetor<Associacao<C,V>> vetor = this.tabela.obtem(indice);
		for(int i=0;i < vetor.tamanho();i++){
			final Associacao<C,V> associacao = vetor.obtem(i);
			if(associacao.getChave().equals(chave)){
				vetor.remove(i);
				this.quantidade--;
				return;
			}
		}
		throw new IllegalArgumentException("nao existe heroi com esta QRcode");
	}

	public V obtem(C chave) {
			final int indice  = this.calculaIndiceDaTabela(chave);
			final Vetor<Associacao<C,V>> vetor = this.tabela.obtem(indice);
			
			for(int i =0 ;i<vetor.tamanho();i++){
				final Associacao<C,V> associacao = vetor.obtem(i);
				if(associacao.getChave().equals(chave))
					return associacao.getValor();
			}
			
			throw new IllegalArgumentException("Nao existe Heroi com estechave");
	}

	public boolean contem(C chave) {
		final int indice  = this.calculaIndiceDaTabela(chave);
		
		final Vetor<Associacao<C,V>> vetor = this.tabela.obtem(indice);
		
		for (int i = 0; i < vetor.tamanho(); i++) {
			final Associacao<C,V> associacao = vetor.obtem(i);
			if(associacao.getChave().equals(chave))
				return true;
		}
		return false;
		
	}

	public int tamanho() {
		return this.quantidade;
	}
	
	public void imprimir() {
		
		System.out.println("CAPACIDADE"+ this.tamanho());
		
		for (int i = 0; i < this.tabela.tamanho(); i++) {
			final Vetor<Associacao<C,V>> objeto = this.tabela.obtem(i);
			
			if(objeto.tamanho() == 0)
				continue;
			
			System.out.println("CODIGO"+ i + "----TOTAL"+ objeto.tamanho());
			
			for (int j = 0; j < objeto.tamanho(); j++) {
				System.out.println(objeto.obtem(j));
			}
			
		}
	}

}
