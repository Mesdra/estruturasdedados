package br.edu.udc.mapa.testes;



import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.mapa.Mapa;


public class MapaTestes {
	
	
	@Test
	public void testeAdiciona() {
		final Mapa<String,Long> mapa = new Mapa<>();
		mapa.adiciona("chavoso", 1L);
		mapa.adiciona("chave 2", 2L);
		
		Assert.assertEquals(mapa.tamanho(),2);
	}
}
