package br.edu.udc.mapa.heroi;

import br.edu.udc.ed.vetor.Vetor;

public class MapaTabelaHeroi {
	private Vetor<Vetor<AssociacaoHeroi>> tabela = new Vetor<>();
	@SuppressWarnings("unused")
	private int quantidade = 0 ;
	
	public MapaTabelaHeroi(){
		for (int i = 0; i < 100; i++) {
			this.tabela.adicionar(new Vetor<AssociacaoHeroi>());
		}
	}
	

	private int calculaIndiceDaTabela(String qrcode){
		return Math.abs(qrcode.hashCode()) % this.tabela.tamanho();
	}

	
	public void adiciona(String qrcode, Heroi heroi) {
		if(this.contem(qrcode))
			this.remove(qrcode);
		
		final int indice = this.calculaIndiceDaTabela(qrcode);
		final Vetor<AssociacaoHeroi>vetor = this.tabela.obtem(indice);
		vetor.adicionar(new AssociacaoHeroi(qrcode, heroi));
		this.quantidade++;
	}

	public void remove(String qrcode) {
		final int indice = this.calculaIndiceDaTabela(qrcode);
		
		final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
		for(int i=0;i < vetor.tamanho();i++){
			final AssociacaoHeroi associacao = vetor.obtem(i);
			if(associacao.getQrcode().equals(qrcode)){
				vetor.remove(i);
				this.quantidade--;
				return;
			}
		}
		throw new IllegalArgumentException("nao existe heroi com esta QRcode");
	}

	public Heroi obtem(String qrcode) {
			final int indice  = this.calculaIndiceDaTabela(qrcode);
			final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
			
			for(int i =0 ;i<vetor.tamanho();i++){
				final AssociacaoHeroi associacao = vetor.obtem(i);
				if(associacao.getQrcode().equals(qrcode))
					return associacao.getHerio();
			}
			
			throw new IllegalArgumentException("Nao existe Heroi com esteqrcode");
	}

	public boolean contem(String qrcode) {
		final int indice  = this.calculaIndiceDaTabela(qrcode);
		
		final Vetor<AssociacaoHeroi> vetor = this.tabela.obtem(indice);
		
		for (int i = 0; i < vetor.tamanho(); i++) {
			final AssociacaoHeroi associacao = vetor.obtem(i);
			if(associacao.getQrcode().equals(qrcode))
				return true;
		}
		return false;
		
	}

	public int tamanho() {
		return this.quantidade;
	}

}
