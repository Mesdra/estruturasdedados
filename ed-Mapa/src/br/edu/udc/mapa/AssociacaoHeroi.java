package br.edu.udc.mapa;


class Associacao<C,V> {
		private C chave;
		private V valor;
		
	
		public Associacao(C chave, V valor ){
			this.chave = chave;
			this.valor = valor;
		}


		public C getChave() {
			return chave;
		}

		public V getValor() {
			return valor;
		}

		
}
