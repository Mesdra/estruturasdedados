package primeiroCap;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class TestesJunit {
	
	@SuppressWarnings("deprecation")
	@Test
	public void testeMetodoElinimiEspaco(){
		final Texto texto = new Texto();
		texto.texto = "palavra com espaco";
		
		texto.tiraEspaço();
		
		Assert.assertEquals(texto.texto, "palavracomespaco");
		
	}
	
	@Test
	public void verCaracterNoIndice(){
		final Texto texto = new Texto();
		texto.texto = "palavra com espaco";
		char caracter ;
		int posicao = 4;
		caracter = texto.buscaPosicao(posicao);
		
		Assert.assertEquals(caracter,'v');
	}
	
	@Test
	public void testeArrayDeStringEspaco(){
		final Texto texto = new Texto();
		texto.texto = "palavra com espaco";
		String[] retorno = texto.separaPor(" ");
		
		Assert.assertEquals(retorno.length,3);
		
	}
	
	@Test
	public void testeArrayDeStringCaracteres(){
		final Texto texto = new Texto();
		texto.texto = "palavra com espaco";
		String[] retorno = texto.separaPor("com");
		
		Assert.assertEquals(retorno.length,2);
		
	}
	
	@Test
	public void testeTerminaComCaracter(){
		final Texto texto = new Texto();
		texto.texto =  "texto";		
		Assert.assertTrue(texto.terminaCom("o"));
	}

}
