package primeiroCap;

public class Texto {
	String texto = new String();

	public void tiraEspaço() {

		this.texto = this.texto.replaceAll(" ", "");
	}

	public char buscaPosicao(int posicao) {
		final char caracter;
		caracter = this.texto.charAt(posicao);

		return caracter;
	}

	public String[] separaPor(String string) {

		String[] retornoSplit = texto.split(string);

		return retornoSplit;
	}

	public boolean terminaCom(String string) {

		int i = this.texto.indexOf(string);

		if (i == this.texto.length()-1)
			return true;

		return false;
	}

}
