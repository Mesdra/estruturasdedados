package terceiroCap;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class JunitTest {

	
	
		@Test
		public void adiciona10000000Carros(){
			Veiculos veiculos = new Veiculos();
			
			for(int i = 0 ;i < 10000000;i ++ ){
				veiculos.adicionarCarro("fiat", "uno");
			}
			
			Assert.assertEquals(veiculos.tamanho(), 10000000);
		}
		@Test
		public void adiciona10000000MaisUmCarrosnaPosicao1000(){
			Veiculos veiculos = new Veiculos();
			
			for(int i = 0 ;i < 10000000;i ++ ){
				veiculos.adicionarCarro("fiat", "uno");
			}
			veiculos.adicionarCarroNaPosicao("carro", "qualquer",1000);
			Assert.assertEquals(veiculos.tamanho(), 10000001);
		}
		
		@Test
		public void adiciona10000000CarrosArrayList(){
			VeiculoaArray veiculos = new VeiculoaArray();
			
			for(int i = 0 ;i < 10000000;i ++ ){
				veiculos.adicionarCarro("fiat", "uno");
			}
			
			Assert.assertEquals(veiculos.tamanho(), 10000000);
		}
		@Test
		public void adiciona10000000MaisUmCarrosnaPosicao1000ArrayList(){
			VeiculoaArray veiculos = new VeiculoaArray();
			
			for(int i = 0 ;i < 10000000;i ++ ){
				veiculos.adicionarCarro("fiat", "uno");
			}
			veiculos.adicionarCarroNaPosicao("carro", "qualquer",1000);
			Assert.assertEquals(veiculos.tamanho(), 10000001);
		}
}
