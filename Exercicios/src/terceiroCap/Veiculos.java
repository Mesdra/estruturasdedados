package terceiroCap;
 


public class Veiculos {
	private Vetor<String> modelo = new Vetor();
	private Vetor<String> marca = new Vetor();
	private int tamanho =0;
	
	public void adicionarCarro(String modelo,String marca){
		this.marca.adicionar(marca);
		this.modelo.adicionar(modelo);
		tamanho++;
	}

	public Object tamanho() {
		return this.tamanho;
	}

	public void adicionarCarroNaPosicao(String modelo, String marca, int i) {
		this.marca.adicionar(i, marca);
		this.modelo.adicionar(i,modelo);
		tamanho++;
		
	}
}
