package quartoCap;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class testesJunit {
	
	@Test
	public void adicionaUmProdutoATabela(){
		Tabela<Produto> produtos = new Tabela();
		Produto produto = new Produto();
		produto.setNome("melancia");
		produto.setDescrišao("vermelha");
		produto.setQuantidade(50);
		
		produtos.adiciona(produto);
		
		Assert.assertNotNull(produtos);
		Assert.assertTrue(produtos.contem(produto));
	}
	
	@Test
	public void adicionaMaisDeUmProduoATabela(){
		Tabela<Produto> produtos = new Tabela();
		 final Produto produto = new Produto();
		produto.setNome("melancia");
		produto.setDescrišao("vermelha");
		produto.setQuantidade(50);
		
		produtos.adiciona(produto);
		final Produto produto2 = new Produto();
		produto2.setNome("abacate");
		produto2.setDescrišao("verde");
		produto2.setQuantidade(700);
		
		produtos.adiciona(produto2);
		Assert.assertNotNull(produtos);
		Assert.assertTrue(produtos.contem(produto));
		Assert.assertTrue(produtos.contem(produto2));
	}
	
	@Test
	public void RemoveUmProduto(){
		Tabela<Produto> produtos = new Tabela();
		 final Produto produto = new Produto();
		produto.setNome("melancia");
		produto.setDescrišao("vermelha");
		produto.setQuantidade(50);
		produtos.adiciona(produto);
		produtos.remove(produto);
		
		
		Assert.assertFalse(produtos.contem(produto));

		
	}
	
	@Test
	public void RemoveDoisProduto(){
		Tabela<Produto> produtos = new Tabela();
		 final Produto produto = new Produto();
		produto.setNome("melancia");
		produto.setDescrišao("vermelha");
		produto.setQuantidade(50);
		
		produtos.adiciona(produto);
		final Produto produto2 = new Produto();
		produto2.setNome("abacate");
		produto2.setDescrišao("verde");
		produto2.setQuantidade(700);
		
		produtos.adiciona(produto2);
		
		produtos.remove(produto);
		produtos.remove(produto2);
		
		Assert.assertFalse(produtos.contem(produto));
		Assert.assertFalse(produtos.contem(produto2));
		
	}
	@Test(expected=RuntimeException.class)
	public void RemoveUmProdutoQueNaoExixte(){
		Tabela<Produto> produtos = new Tabela();
		 final Produto produto = new Produto();
	
		 produtos.remove(produto);
		
		Assert.fail("nao pode passar");

		
	}
	
	
}
